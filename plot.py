#!/usr/bin/env python
from argparse import ArgumentParser
import subprocess
import matplotlib
matplotlib.use('TKAgg')
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import drive
from drive import DEFAULT

def plot(out):
    lines = out.split("\n")
    y = []
    t = []
    sns.set(style="ticks")
    print("         driverFrq   resistanceFrq")
    for line in lines:
        print(line)
        if line.startswith("#OUTPUT:"):
            print(line)
            y.append(float(line.split()[1]))
            t.append("driver")
            y.append(float(line.split()[2]))
            t.append("resistance")
    d = {'generations' : range(0, len(y)), 'allele frequency' : y, 'allele' : t}
    df = pd.DataFrame(d)
    g = sns.lmplot(x="generations", y="allele frequency", data=df, hue="allele", fit_reg=False)
    g.set(xlim=(0, len(y)), ylim=(0, 1))
    sns.plt.show()

def main(args):
    output = drive.generate(args)
    with open("tmp.txt", "w") as f:
        f.write(output)
    process = subprocess.Popen(["slim", "tmp.txt"], stdout=subprocess.PIPE, stderr=subprocess.PIPE,universal_newlines=True)
    out, err = process.communicate()
    plot(out)

if __name__=="__main__":
    parser = ArgumentParser(description='Process some integers.')
    parser.add_argument('-N', dest='N', type=int, default=DEFAULT["N"], help="Census Population Size, default=" + str(DEFAULT["N"]))
    parser.add_argument('-Ne', dest='Ne', type=int, default=DEFAULT["Ne"], help="Variance effective population size, default=" + str(DEFAULT["N"]))
    parser.add_argument('-sd0', dest='Sd0', type=float, default=DEFAULT["Sd0"], help="Fitness cost of driver/wildtype heterozygotes, default=" + str(DEFAULT["Sd0"]))
    parser.add_argument('-sdr', dest='Sdr', type=float, default=DEFAULT["Sdr"], help="Fitness cost of driver/resistance heterozygotes, default=" + str(DEFAULT["Sdr"]))
    parser.add_argument('-sr0', dest='Sr0', type=float, default=DEFAULT["Sr0"], help="Fitness cost of resistance/wildtype heterozygotes, default=" + str(DEFAULT["Sr0"]))
    parser.add_argument('-sdd', dest='Sdd', type=float, default=DEFAULT["Sdd"], help="Fitness cost of driver homozygotes, default=" + str(DEFAULT["Sdd"]))
    parser.add_argument('-srr', dest='Srr', type=float, default=DEFAULT["Srr"], help="Fitness cost of resistance homozygotes, default=" + str(DEFAULT["Srr"]))
    parser.add_argument('-L', dest='L', type=int, default=DEFAULT["L"], help="Chromosome Length=, default=" + str(DEFAULT["L"]))
    parser.add_argument('-c', dest='C', type=float, default=DEFAULT["C"], help="MCR converseion rate, default=" + str(DEFAULT["C"]))
    parser.add_argument('-r', dest='R', type=float, default=DEFAULT["R"], help="Recombination rate, default=" + str(DEFAULT["R"]))
    parser.add_argument('-d', dest='D', type=float, default=DEFAULT["D"], help="Fraction of cases in which repair generates resistance allele by NHEJ, default=" + str(DEFAULT["D"]))
    parser.add_argument('-mu', dest='MU', type=float, default=DEFAULT["MU"], help="Rate at which wildtype allele mutates into resistance allele, default=" + str(DEFAULT["MU"]))
    parser.add_argument('-G', dest='G', type=int, default=DEFAULT["G"], help="Number of generations, default=" + str(DEFAULT["G"]))
    parser.add_argument('-xd', dest='Xd', type=float, default=DEFAULT["Xd"], help="Introduction frequency of driver allele, default=" + str(DEFAULT["Xd"]))
    parser.add_argument('-xr', dest='Xr', type=float, default=DEFAULT["Xr"], help="Introduction frequency of resistance allele, default=" + str(DEFAULT["Xr"]))
    parser.add_argument('-drive_sex_bias', dest='SB', action='store_true', default=DEFAULT["DriveSexBias"], help="Flag to signify that the drive only occures in one sex")
    parser.add_argument('-cleave_homo_leathal', dest='HL', action='store_true', default=DEFAULT["HL"], help="Flag to signify that in the presence of a driver gamete, the wildtype is homoleathal")
    args = parser.parse_args()
    main(args)
