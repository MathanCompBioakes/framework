#!/usr/bin/env python
from argparse import ArgumentParser
from math import floor, sqrt

DEFAULT = {
        "N": 10000,
        "Ne": 10000,
        "Sd0": 0.0,
        "Sdr": 0.0,
        "Sdd": 0.0,
        "Sr0": 0.0,
        "Srr": 0.0,
        "L": 10,
        "C":0.9,
        "R":0.0,
        "D":0.000001,
        "MU":0.00000001,
        "Xd": 0.00001,
        "Xr": 0.0,
        "G": 10000000,
        "Gamete": False,
        "DriveSexBias": False,
        "HL": False
        }

class params:
    def __init__(self, N=DEFAULT["N"], Sd0=DEFAULT["Sd0"], Sdr=DEFAULT["Sdr"], Sdd=DEFAULT["Sdd"],
            Sr0=DEFAULT["Sr0"], Srr=DEFAULT["Srr"], L=DEFAULT["L"], C=DEFAULT["C"], R=DEFAULT["R"],
            D=DEFAULT["D"], MU=DEFAULT["MU"], G=DEFAULT["G"], Xd=DEFAULT["Xd"], Xr=DEFAULT["Xr"],
            Ne=DEFAULT["N"], SB=DEFAULT["DriveSexBias"], HL=DEFAULT["HL"]):
        self.N = N
        self.Ne = Ne
        self.Sd0 = Sd0
        self.Sdr = Sdr
        self.Sdd = Sdd
        self.Sr0 = Sr0
        self.Srr = Srr
        self.L = L
        self.C = C
        self.R = R
        self.D =D
        self.MU = MU
        self.G = G
        self.Xd = Xd
        self.Xr = Xr
        self.SB = SB
        self.HL = HL
        self.Gamete = Gamete

def init(args):
    xd = floor(args.N*2*args.Xd)-1
    count = 1
    out ='    initialize() {\n\
        initializeMutationRate( 0.0 );\n\
        initializeMutationType("m2", 0.5, "f", 0.0);\n\
        initializeMutationType("m3", 0.5, "f", 0.0);\n'
    if args.HL:
        out += '        initializeMutationType("m4", 0.5, "f", 0.0);\n'
    out += '        initializeGenomicElementType("g1", c(m2, m3), c(1.0, 1.0));\n\
        initializeGenomicElement(g1, 0, ' + str(args.L) + ');\n\
        initializeRecombinationRate(' + str(args.R) + ');\n\
    }\n\
    1 {\n\
        sim.addSubpop("p1", ' +  str(args.N) + ' );\n\
    }\n\
    1 late() {\n\
        mut = p1.genomes[0].addNewDrawnMutation(m2, 5);\n'
    if args.HL:
        xd *= 2
        while xd >= 999999:
            out += 'sample(p1.genomes[' + str(count) + ':' + str(999998 + count) + '],' + str(500000)  + ').addMutations(mut);\n'
            xd -= 999999
            count += 999999
        out += 'sample(p1.genomes[' + str(count) + ':' + str(min(count + xd, args.N*2-1)) + '],' + str(floor(xd/2)) + ').addMutations(mut);\n'

    else:
        while xd >= 999999:
            out += 'p1.genomes[' + str(count) + ':' + str(999998 + count) + '].addMutations(mut);\n'
            xd -= 999999
            count += 999999
        out += 'p1.genomes[' + str(count) + ':' + str(count + xd) + '].addMutations(mut);\n'
    if args.Xr > 0:
        out += '        targets = sample(p1.genomes[' + str(floor(args.N*2*args.Xd)) + ':' + str(1000000 - floor(args.N*2*args.Xd)) + '], ' + str(floor(args.N*2*args.Xr)) + ');\n\
                mut = targets[0].addNewDrawnMutation(m3, 7);\n\
                targets[1:' + str(floor(args.N*2*args.Xr)-1) + '].addMutations(mut);\n'
    out += '    }\n'
    return out

def home(genomeName, args):
    out = '     driver = sim.mutationsOfType(m2);\n\
       resistance = sim.mutationsOfType(m3);\n\
       genomePair = c(' + genomeName + '1,' +  genomeName + '2 );\n\
       driverCount = 0;\n\
       resistanceCount = 0;\n\
       driverCount = sum(genomePair.countOfMutationsOfType(m2));\n\
       hasMutOnChromosome1 = ' + genomeName + '1.containsMutations(driver);\n\
       hasMutOnChromosome2 = ' + genomeName + '2.containsMutations(driver);\n\
       if(size(resistance) > 0) {\n\
           resistanceCount = sum(genomePair.countOfMutationsOfType(m3));\n\
           hasMutOnChromosome1 = (' + genomeName  + '1.countOfMutationsOfType(m3)>0);\n\
           hasMutOnChromosome2 = (' + genomeName  + '2.countOfMutationsOfType(m3)>0);\n\
        }\n\
        if ((driverCount + resistanceCount) < 2) {\n\
           hasWildOnChromosome1 = 1;\n\
           hasWildOnChromosome2 = 1;\n\
           if ( driverCount == 1) {\n\
               hasWildOnChromosome1 = !' + genomeName + '1.containsMutations(driver);\n\
               hasWildOnChromosome2 = !' + genomeName + '2.containsMutations(driver);\n\
           }\n\
           if ( resistanceCount == 1) {\n\
               hasWildOnChromosome1 = (hasWildOnChromosome1 & !any(' + genomeName + '1.containsMutations(resistance)));\n\
               hasWildOnChromosome2 = (hasWildOnChromosome2 & !any(' + genomeName + '2.containsMutations(resistance)));\n\
           }\n\
           if (hasWildOnChromosome1 & (runif(1) < ' + str(args.MU) + ')) {\n\
               mut = ' + genomeName +'1.addNewDrawnMutation(m3, 7);\n\
               ' + genomeName + '1.addMutations(mut);\n\
               resistanceCount = resistanceCount + 1;\n\
           }\n\
           if (hasWildOnChromosome2 & (runif(1) < ' + str(args.MU) + ' )) {\n\
               mut = ' + genomeName + '2.addNewDrawnMutation(m3, 7);\n\
               ' + genomeName + '2.addMutations(mut);\n\
               resistanceCount = resistanceCount + 1;\n\
           }\n\
       }\n\
       if ((driverCount == 1) & (resistanceCount == 0)) {\n'
    if args.SB:
        out += '           if(runif(1) <= 0.5) {\n'
    out += '           hasMutOnChromosome1 = ' + genomeName + '1.containsMutations(driver);\n\
               hasMutOnChromosome2 = ' + genomeName + '2.containsMutations(driver);\n'
    if args.HL:
        out += '                   lethal = sim.mutationsOfType(m4);\n\
                           if (hasMutOnChromosome1 & !hasMutOnChromosome2) {\n\
                                   mut = ' + genomeName + '2.addNewDrawnMutation(m4, 3);\n\
                                   ' + genomeName + '2.addMutations(mut);\n\
                           } else if (hasMutOnChromosome2 & !hasMutOnChromosome1) {\n\
                                   mut = ' + genomeName + '1.addNewDrawnMutation(m4, 3);\n\
                                   ' + genomeName + '1.addMutations(mut);\n\
                           }\n\
                       }\n\
                   //}\n\
              //}\n'
    elif args.Gamete:
     out += '       if(parent1Genome1.countOfMutationsOfType(m2) | parent1Genome2.countOfMutationsOfType(m2)){\n\
                        if(parent2Genome1.countOfMutationsOfType(m2) | parent2Genome2.countOfMutationsOfType(m2)){\n\
                            if (hasMutOnChromosome1 & !hasMutOnChromosome2) {\n\
                               ' + genomeName + '2.addMutations(driver);\n\
                               driverCount = driverCount + 1;\n\
                            } else if (hasMutOnChromosome2 & !hasMutOnChromosome1) {\n\
                               ' + genomeName  + '1.addMutations(driver);\n\
                               driverCount = driverCount + 1;\n\
                            }\n\
                        }\n\
                    } else if (parent2Genome1.countOfMutationsOfType(m2) | parent2Genome2.countOfMutationsOfType(m2)) {\n\
                            if (hasMutOnChromosome1 & !hasMutOnChromosome2) {\n\
                               ' + genomeName + '2.addMutations(driver);\n\
                               driverCount = driverCount + 1;\n\
                            } else if (hasMutOnChromosome2 & !hasMutOnChromosome1) {\n\
                               ' + genomeName  + '1.addMutations(driver);\n\
                               driverCount = driverCount + 1;\n\
                            }\n\
                    }\n\
               }\n'
    else:
     out += '         if(runif(1) < ' + str(args.C) + ') {\n\
                   if(runif(1) < ' + str(args.D) + ') {\n\
                       if (hasMutOnChromosome1 & !hasMutOnChromosome2) {\n\
                           mut = ' + genomeName + '2.addNewDrawnMutation(m3, 7);\n\
                           ' + genomeName + '2.addMutations(mut);\n\
                           resistanceCount = resistanceCount + 1;\n\
                       } else if (hasMutOnChromosome2 & !hasMutOnChromosome1) {\n\
                           mut = ' + genomeName + '1.addNewDrawnMutation(m3, 7);\n\
                           ' + genomeName + '1.addMutations(mut);\n\
                           resistanceCount = resistanceCount + 1;\n\
                       }\n\
                   } else {\n\
                        if (hasMutOnChromosome1 & !hasMutOnChromosome2) {\n\
                               ' + genomeName + '2.addMutations(driver);\n\
                               driverCount = driverCount + 1;\n\
                           } else if (hasMutOnChromosome2 & !hasMutOnChromosome1) {\n\
                               ' + genomeName  + '1.addMutations(driver);\n\
                               driverCount = driverCount + 1;\n\
                           }\n\
                       }\n\
                   }\n\
               }\n'
    if args.SB:
        out += '       }\n'
    return out

def fitness(args):
    out = ''
    if args.HL:
            out += '    fitness(m4) {\n\
               if(homozygous)\n\
                   return 0.0;\n\
               else\n\
                   return ' + str(1 - args.Sd0) + ';\n\
            }\n'
    out += '     fitness(m2) {\n\
        if(homozygous)\n\
                return ' +  str(1 - args.Sdd) + ';\n\
        else if(sum(c(genome1, genome2).countOfMutationsOfType(m3)))\n\
                return ' + str(1 - args.Sdr) + ';\n\
        else\n\
                return ' + str(1 - args.Sd0) + ';\n\
    }\n'
    if args.Srr > 0 or args.Sr0 > 0:
         out += '    fitness(m3) {\n\
        if(homozygous)\n\
                return ' + str(1 - args.Srr) + ';\n\
        else\n\
                return ' + str(1 - args.Sr0) + ';\n\
    }\n'
    return out

def output(args):
    return "    1:" + str(args.G) + ' late() {\n\
            //if (sim.countOfMutationsOfType(m2) == 0){\n\
            if ((sim.countOfMutationsOfType(m2) == 0| sum(sim.mutationFrequencies(p1, sim.mutationsOfType(m3)))>0.01)){\n\
                //fixed = (sum(sim.substitutions.mutationType == m2) == 1);\n\
                fixed = (sum(sim.mutationFrequencies(p1, sim.mutationsOfType(m3)))>0.01);\n\
                if(fixed){\n\
                    cat(paste("#OUTPUT: 1 " + sim.generation, "' + str("\\n") + '"));\n\
                } else {\n\
                    cat(paste("#OUTPUT: 0 " + sim.generation, "' + str("\\n") + '"));\n\
                }\n\
                sim.simulationFinished();\n\
            } else {\n\
                driver = sim.mutationsOfType(m2);\n\
                resistance = sim.mutationsOfType(m3);\n\
            if(sim.countOfMutationsOfType(m3) > 0) {\n\
                cat("#OUTPUT: ");\n\
                cat(sim.mutationFrequencies(p1, driver) + "\t" + sum(sim.mutationFrequencies(p1, resistance)) + \"' + str("\\n") + '\");\n\
            } else {\n\
                cat("#OUTPUT: ");\n\
                cat(sim.mutationFrequencies(p1, driver) + "\t" + "0" + \"' + str("\\n") + '\");\n\
            }\n\
        }\n\
    }'


def output2(args):
    return "    1:" + str(args.G) + ' late() {\n\
            //if ((sim.countOfMutationsOfType(m2) == 0| sum(sim.mutationFrequencies(p1, sim.mutationsOfType(m3)))>0.01)){\n\
            if (sim.countOfMutationsOfType(m2) == 0){\n\
                fixed = (sum(sim.substitutions.mutationType == m3) == 1);\n\
                //fixed = (sum(sim.mutationFrequencies(p1, sim.mutationsOfType(m3)))>0.01);\n\
                if(fixed){\n\
                    //cat("#OUTPUT: 1 ' + str("\\n") + '");\n\
                    cat(paste("#OUTPUT: 1 " + sim.generation, "' + str("\\n") + '"));\n\
                } else {\n\
                    //cat("#OUTPUT: 0 ' + str("\\n") + '");\n\
                    cat(paste("#OUTPUT: 0 " + sim.generation, "' + str("\\n") + '"));\n\
                }\n\
                sim.simulationFinished();\n\
            }\n\
    }'

def generate(args):
    return init(args) + '    modifyChild() {' + home("childGenome", args) + '        return T;\n   }\n' + fitness(args) + output(args)

def main(args):
    output = generate(args)
    print(output)


if __name__=="__main__":
    parser = ArgumentParser(description='Process some integers.')
    parser.add_argument('-N', dest='N', type=int, default=DEFAULT["N"], help="Census Population Size, default=" + str(DEFAULT["N"]))
    parser.add_argument('-Ne', dest='Ne', type=int, default=DEFAULT["Ne"], help="Variance effective population size, default=" + str(DEFAULT["N"]))
    parser.add_argument('-sd0', dest='Sd0', type=float, default=DEFAULT["Sd0"], help="Fitness cost of driver/wildtype heterozygotes, default=" + str(DEFAULT["Sd0"]))
    parser.add_argument('-sdr', dest='Sdr', type=float, default=DEFAULT["Sdr"], help="Fitness cost of driver/resistance heterozygotes, default=" + str(DEFAULT["Sdr"]))
    parser.add_argument('-sr0', dest='Sr0', type=float, default=DEFAULT["Sr0"], help="Fitness cost of resistance/wildtype heterozygotes, default=" + str(DEFAULT["Sr0"]))
    parser.add_argument('-sdd', dest='Sdd', type=float, default=DEFAULT["Sdd"], help="Fitness cost of driver homozygotes, default=" + str(DEFAULT["Sdd"]))
    parser.add_argument('-srr', dest='Srr', type=float, default=DEFAULT["Srr"], help="Fitness cost of resistance homozygotes, default=" + str(DEFAULT["Srr"]))
    parser.add_argument('-L', dest='L', type=int, default=DEFAULT["L"], help="Chromosome Length=, default=" + str(DEFAULT["L"]))
    parser.add_argument('-c', dest='C', type=float, default=DEFAULT["C"], help="MCR converseion rate, default=" + str(DEFAULT["C"]))
    parser.add_argument('-r', dest='R', type=float, default=DEFAULT["R"], help="Recombination rate, default=" + str(DEFAULT["R"]))
    parser.add_argument('-d', dest='D', type=float, default=DEFAULT["D"], help="Fraction of cases in which repair generates resistance allele by NHEJ, default=" + str(DEFAULT["D"]))
    parser.add_argument('-mu', dest='MU', type=float, default=DEFAULT["MU"], help="Rate at which wildtype allele mutates into resistance allele, default=" + str(DEFAULT["MU"]))
    parser.add_argument('-G', dest='G', type=int, default=DEFAULT["G"], help="Number of generations, default=" + str(DEFAULT["G"]))
    parser.add_argument('-xd', dest='Xd', type=float, default=DEFAULT["Xd"], help="Introduction frequency of driver allele, default=" + str(DEFAULT["Xd"]))
    parser.add_argument('-xr', dest='Xr', type=float, default=DEFAULT["Xr"], help="Introduction frequency of resistance allele, default=" + str(DEFAULT["Xr"]))
    parser.add_argument('-sb', dest='SB', action='store_true', default=DEFAULT["DriveSexBias"], help="Flag to signify that the drive only occures in one sex")
    parser.add_argument('-hl', dest='HL', action='store_true', default=DEFAULT["HL"], help="Flag to signify that after encountering a driver gamete, the wildtype is homoleathal")
    parser.add_argument('-gamete', dest='Gamete', action='store_true', default=DEFAULT["Gamete"], help="Flag to signify that homing occurs in gametes")
    args = parser.parse_args()
    main(args)
